# sylva-snmp-resources

This chart takes values from the snmp auth defined in sylva-core and produces the configuration file for snmp-exporter

Input values are in the form of:
```yaml
auth:
  snmpv3:
    version: 3
    security_level: authPriv
    username: snmp
    password: pass
    auth_protocol: MD5
    priv_protocol: AES
    priv_password: priv_pass
  snmpv2:
    version: 2
    community: public
```

The authentication part is concatenated with the OIDs files in the `files` folder:

```yaml
auth:
  snmpv3:
    version: 3
    security_level: authPriv
    username: snmp
    password: pass
    auth_protocol: MD5
    priv_protocol: AES
    priv_password: priv_pass
  snmpv2:
    version: 2
    community: public
modules:
  dell_idrac:
    walk:
      # MGMT
      - 1.3.6.1.2.1.1 # system                          # SNMP MIB-2
      - 1.3.6.1.2.1.2 # interfaces                      # SNMP MIB-2
      - 1.3.6.1.2.1.31.1.1 # ifMIB                           # IF-MIB
      - 1.3.6.1.2.1.4.22.1 # ipNetToMediaIfIndex             # IP-MIB
      - 1.3.6.1.2.1.47.1.1.1.1 # entPhysicalTable                # ENTITY-MIB.mib
      [...]
  hp_cpq:
    walk:
      # MGMT
      - 1.3.6.1.2.1.1 # system                                # SNMP MIB-2
      - 1.3.6.1.2.1.2 # interfaces                            # SNMP MIB-2
      - 1.3.6.1.2.1.31.1.1 # ifMIB                                 # IF-MIB
      - 1.3.6.1.2.1.4.22.1 # ipNetToMediaIfIndex                   # IP-MIB
      - 1.3.6.1.2.1.47.1.1.1.1 # entPhysicalTable                      # ENTITY-MIB.mib
      [...]
```
